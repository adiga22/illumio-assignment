# import illumio
from illumio import Firewall






class Test:
    def setup(self):
        self.f = Firewall("fw.csv")
        self.f.populate_rules()

    def test_input1(self):
        
        assert self.f.accept_packet("inbound", "tcp", 80, "192.168.1.2") == True


    def test_input2(self):
        
        assert self.f.accept_packet("inbound", "udp", 53, "192.168.2.1") == True



    def test_input3(self):
        
        assert self.f.accept_packet("inbound", "tcp", 81, "192.168.1.2") == False


    def test_input4(self):
       
        assert self.f.accept_packet("inbound", "udp", 24, "52.12.48.92") == False

    def test_input5(self):
       
        assert self.f.accept_packet("inbound", "udp", 10000, "192.168.10.11") == False

    def test_input6(self):
       
        assert self.f.accept_packet("outbound", "udp", 1000, "52.12.48.92") == True

