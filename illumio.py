import csv

"""Rule class holds the bleprint for the Rule objects containing 
direction, protocol, port and ip adrress"""
class Rule:

	def __init__(self,direction,protocol,port,ip_address):
		
		self.direction = direction
		self.protocol = protocol
		self.port = port
		self.ip_address = ip_address
	

	def __repr__(self):  
		return str(self.__dict__)

"""Firewall class implementation as per details shared in the pdf """
class Firewall: 
	def __init__(self,filePath):
		self.filePath = filePath
		self._ruleList = list()

	"""populates the rules into an in memory data structure to be used for checking the rule conditions """
	def populate_rules(self):
		with open(self.filePath,"r" ) as csv:
			rules = csv.readlines()
		for rule in rules:
			rule_array = rule.strip().split(",")
			r = Rule(rule_array[0],rule_array[1],rule_array[2],rule_array[3])
			self._ruleList.append(r)

	"""Private method starting with "_" .Checks if the IP address or PORT is a range of numbers or a single number"""
	def _isRange(self,arg):
		if("-" in arg.strip()):
			return True
		return False

	"""Private method .Returns True if the input port is in the admissible range for that rule """
	def _comparePort(self,port,lower,upper):
		
		if(port>=int(lower) and port <=int(upper)):
			return True
		return False
	
	"""Private Method. Compares the IP address in input is within the admissible range for that rule
		Returns true if within range, else returns False
	"""
	def _compareIP(self,ip,rule_ip):

		#if the rule ip_address is a range , check if the given ip lies within it by comaparing each subnet
		if(self._isRange(rule_ip)):
			s = rule_ip.split('-')
			min_ip, max_ip = [[int(i) for i in j.split('.')] for j in s]
			ip = ip.split('.')
			for ind, val in enumerate(ip):
				if (int(val) < min_ip[ind] or int(val) > max_ip[ind]):
					return False
		#if the rule contains a single IP address, compare directly with the input IP
		else:
			if(ip != rule_ip):
				return False
		
		return True

	"""returns true if a packet is admissible according to any of the rules, False otherwise"""
	def accept_packet(self,direction,protocol,port,ip_address):

		#optimisation based on direction and protocol, filter out the rules for the selected dir and protocol only
		filtered_rules = list(filter(lambda x: x.direction == direction and x.protocol == protocol , self._ruleList ))
		
		#check each filtered rule for ip_address and port matches 
		for rule in filtered_rules:
			if(self._isRange(rule.port)):
				port_split = rule.port.strip().split("-")
				lower_port = port_split[0]
				upper_port = port_split[1]
			else :
				lower_port = upper_port = rule.port

			if(self._compareIP(ip_address, rule.ip_address) and self._comparePort(port,lower_port,upper_port)):
				return True


		#if none of the rules match, return False
		return False

if __name__ == "__main__":
	f = Firewall("fw.csv")
	f.populate_rules()
	print(f.accept_packet("outbound", "udp", 1000, "52.12.48.92"))

